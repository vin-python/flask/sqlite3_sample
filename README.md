python3.6

virtualenv -p python3.6 venv

python -m flask --version

--------------------
Run flask app
1: 
    export FLASK_ENV=development
    flask run
2:
    export FLASK_APP=app
    flask run
3:
    python nameof_app_file.py
    if __name__ == "__main__":
        app.run(host='127.0.0.1', port=5002)
        app.debug(True)


